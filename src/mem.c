#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool is_block_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool

region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    size_t size = region_actual_size(query + offsetof(struct block_header, contents));
    bool extends = true;

    struct block_header *region_starts = map_pages(addr, size, MAP_FIXED_NOREPLACE);

    if (region_starts == MAP_FAILED) {
        region_starts = map_pages(addr, size, 0);
        if (region_starts == MAP_FAILED)
            return REGION_INVALID;
        extends = false;
    }

    block_init(region_starts, (block_size) {.bytes = size}, NULL);
    return (struct region) {
            .addr = region_starts,
            .size = size,
            .extends = extends
    };
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header *first_of_region = HEAP_START;
    while (first_of_region) {
        struct block_header *current_block = first_of_region;
        struct block_header *region = first_of_region;
        size_t region_size = 0;
        while (current_block) {
            region_size += size_from_capacity(current_block->capacity).bytes;
            if (current_block->next == block_after(current_block)) {
                current_block = current_block->next;
            } else {
                first_of_region = current_block->next;
                break;
            }
        }
        munmap(region, round_pages(region_size));
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free && query + offsetof(
            struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {

        block_size first_block_size = size_from_capacity((block_capacity) {.bytes = query});
        block_size second_block_size = size_from_capacity(
                (block_capacity) {block->capacity.bytes - first_block_size.bytes});

        struct block_header *second_block = (void *) block + first_block_size.bytes;
        block_init(second_block, second_block_size, block->next);

        block->capacity.bytes = query;
        block->next = second_block;
        return true;
    }
    return false;

}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return fst && (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst && snd && fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}


static bool try_merge_with_next(struct block_header *block) {
    struct block_header *next = block->next;
    if (mergeable(block, next)) {
        block->capacity.bytes += (size_from_capacity(next->capacity)).bytes;
        block->next = next->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_header *current = block;
    struct block_header *last = block;

    while (current) {
        if (current->is_free) {
            while (try_merge_with_next(current)) {};
            if (is_block_big_enough(sz, current))
                return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = current};
        }
        last = current;
        current = current->next;
    }

    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = last};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result search_result = find_good_or_last(block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(search_result.block, query);
        search_result.block->is_free = false;
    }
    return search_result;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    struct region region = alloc_region(block_after(last), query);
    last->next = region.addr;
    return try_merge_with_next(last) ? last : last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result search_result = try_memalloc_existing(query, heap_start);
    if (search_result.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header *last = search_result.block;
        struct block_header *first_of_new_region = grow_heap(last, query);
        if (!first_of_new_region) return NULL;
        search_result = try_memalloc_existing(query, first_of_new_region);
    }
    if (search_result.type == BSR_FOUND_GOOD_BLOCK)
        return search_result.block;

    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
            struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header)) {};
}
