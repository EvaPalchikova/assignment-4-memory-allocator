#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>


struct test_structure {
    int64_t int64;
};

int main() {
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    printf("test 1: Обычное успешное выделение памяти\n");
    struct test_structure **test_structures = _malloc(sizeof(struct test_structure *) * 5);
    for (size_t i = 0; i < 5; i++) {
        test_structures[i] = _malloc(sizeof(struct test_structure));
        test_structures[i]->int64 = (int64_t) i;
    }
    debug_heap(stdout, heap);

    printf("test 2: Освобождение одного блока из нескольких выделенных\n");
    _free(test_structures[3]);
    debug_heap(stdout, heap);

    printf("test 3: Освобождение двух блоков из нескольких выделенных\n");
    _free(test_structures[1]);
    _free(test_structures[2]);
    debug_heap(stdout, heap);
    heap_term();

    printf("test 4: Память закончилась, новый регион памяти расширяет старый\n");
    heap = heap_init(0);
    _malloc(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    heap_term();

    printf("test 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте\n");
    heap = heap_init(0);
    debug_heap(stdout, heap);
    void *another_region = mmap(heap + REGION_MIN_SIZE + 1, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    _malloc(REGION_MIN_SIZE);
    debug_heap(stdout, heap);
    munmap(another_region, REGION_MIN_SIZE);
    heap_term();

    return 0;
}
